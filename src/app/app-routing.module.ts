import { NgModule } from '@angular/core';
import { SpacexdataComponent } from './spacexdata/spacexdata.component';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: SpacexdataComponent,
  },
  {
    path: 'searchresult/:launch_year/:launch_success/:land_success',
    component: SpacexdataComponent,
  },
  {
    path: 'searchresult/:type/:value',
    component: SpacexdataComponent,
  },
  {
    path: 'searchresult/:type/:value',
    component: SpacexdataComponent,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
